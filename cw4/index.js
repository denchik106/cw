//1
//Сделайте функцию, которая принимает параметром число от 1 до 7, а возвращает день недели на Украинском языке


function week(a) {
    switch (a) {
        case 1: show('Понеділок');
            break;
        case 2: show('Віторок');
            break;
        case 3: show('Середа');
            break;
        case 4: show('Четвер');
            break;
        case 5: show('Пятниця');
            break;
        case 6: show('Субота');
            break;
        case 7: show('Неділя');
            break;

    }
};

function show(b) {
    document.write(`${b}  </br>`);
};

week(1);
week(2);
week(3);
week(4);
week(5);
week(6);
week(7);

//2
// >Дана строка вида 'var_text_hello'. Сделайте из него текст 'VarTextHello'.

let [...a] = `var_text_hello`;

for (let i = 0; i < a.length; i++) {
    if (a[i] === `_`) {
        a.splice(i, 1)

    }

}
document.write(a.join(``));

//3
//Создайте функцию которая будет заполнять массив 10-ю иксами с помощью цикла.

let arr = [];

function x() {
    for (let i = 0; i < 10; i++) {
        arr.push(`x`);

    }
    return arr;
};



document.write(`<br> ${x().join(`*`)}`);

//4
//Создайте маcсив на 50 элементов и заполните каждый элемент его номером, 
//не используя циклы выведите каждый нечетный элемент в параграфе, а четный в диве.


arr = [];

for (let i = 0; i < 50; i++) {
    arr[i] = i;
}
console.log(arr)


//5
//Если переменная a больше нуля - то в ggg запишем функцию, которая выводит один !, иначе запишем функцию, которая выводит два !

let ggg;

a = 5;

if (a > 0) {
    ggg = show2;
} else {
    ggg = show1;
}

function show2() {
    console.log(`!`)
};

function show1() {
    console.log(`!!`)
};

ggg();

//6
//Функция ggg принимает 2 параметра: анонимную функцию, которая возвращает 3 и анонимную функцию, которая возвращает 4. 
//Верните результатом функции ggg сумму 3 и 4.


function ggg1(a, b) {
    return a + b;
};

const a3 = function () {
    return 3;
};

const b4 = function () {
    return 4;
}


console.log(ggg1(a3(), b4()));

//7
//Сделайте функцию, которая считает и выводит количество своих вызовов.

let counter = 0;

function count() {
    counter += 1;
    console.log(counter);
}

count();
count();
count();
count();
count();